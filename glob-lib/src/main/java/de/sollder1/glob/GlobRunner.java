package de.sollder1.glob;

import java.util.Set;

final class GlobRunner {

    private static final Set<Character> KEY_WORDS = Set.of('?');

    private final String pattern;
    private final String value;

    private int patternIndex = 0;
    private int valueIndex = 0;

    public GlobRunner(String pattern, String value) {
        this.pattern = pattern;
        this.value = value;
    }

    public boolean run() {
        while (patternIndex < pattern.length() && valueIndex < value.length()) {
            char currPattern = pattern.charAt(patternIndex);
            if (KEY_WORDS.contains(currPattern)) {
                if (!handleKeyWord(currPattern)) {
                    return false;
                }
            } else {
                if (value.charAt(valueIndex) != pattern.charAt(patternIndex)) {
                    return false;
                }
                patternIndex++;
                valueIndex++;
            }
        }
        return true;
    }

    private boolean handleKeyWord(char currPattern) {
        switch (currPattern) {
            case '?': {
                patternIndex++;
                valueIndex++;
                return true;
            }
        }
        return false;
    }


}
