package de.sollder1.glob;

//See https://en.wikipedia.org/wiki/Glob_(programming)
public final class Glob {

    private Glob () {
        throw new IllegalArgumentException();
    }

    public static boolean matches(String globPattern, String value) {
        return new GlobRunner(globPattern, value).run();
    }

}
