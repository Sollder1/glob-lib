import de.sollder1.glob.Glob;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GlobTest {
    @Test
    public void test1() {
        shouldMatch("", "");
        shouldMatch("a", "a");
        shouldNotMatch("a", "b");
        shouldMatch("?", "b");


    }

    private void shouldMatch(String globPattern, String value) {
        boolean matches = Glob.matches(globPattern, value);
        assertTrue(matches, "Value '%s' should match pattern '%s'".formatted(value, globPattern));
    }

    private void shouldNotMatch(String globPattern, String value) {
        boolean matches = Glob.matches(globPattern, value);
        assertFalse(matches, "Value '%s' should NOT match pattern '%s'".formatted(value, globPattern));
    }

}
